
var button = document.querySelector('#start-button');
var output = document.querySelector('#output');
var output2 = document.querySelector('#output2');

function func1() {
    return new Promise( function(resolve, reject) {
        setTimeout(function() { 
            resolve('https://swapi.co/api/people/1');
            console.log('resolved1');
        }, 3000);
    });
}

button.addEventListener('click', function() {
  // Create a new Promise here and use setTimeout inside the function you pass to the constructor
    func1()
        .then(promise =>{
            fetch(promise)
                .then((response) => {
                    return response.json();
                })
                .then((response) => {
                    output.innerHTML= `${response.name}`
                    //document.getElementById("output").appendChild(document.createTextNode(response.name));
                })
                .then( fetch('https://httpbin.org/put', {
                    method: 'PUT', body: JSON.stringify({person:{ name: 'Jean Pierre Polnareff', age: 20 }})
                    })
                        .then((response) => {
                            return response.json();
                        })
                        .then((response) => {
                            console.log('response2');
                            output2.innerHTML= `${response.json.person.name}`
                        })
                        .catch(error => output.innerHTML= `error3`)
                )
                .catch(error => output.innerHTML= `error2`)
        })
        .catch(error => output.innerHTML= `error1`)

    
        
  //setTimeout(function() { // <- Store this INSIDE the Promise you created!
    // Resolve the following URL: https://swapi.co/api/people/1
  //}, 3000);

  // Handle the Promise "response" (=> the value you resolved) and return a fetch()
  // call to the value (= URL) you resolved (use a GET request)

  // Handle the response of the fetch() call and extract the JSON data, return that
  // and handle it in yet another then() block

  // Finally, output the "name" property of the data you got back (e.g. data.name) inside
  // the "output" element (see variables at top of the file)



  // Repeat the exercise with a PUT request you send to https://httpbin.org/put
  // Make sure to set the appropriate headers 
  // Send any data of your choice, make sure to access it correctly when outputting it
  // Example: If you send {person: {name: 'Max', age: 28}}, you access data.json.person.name
  // to output the name (assuming your parsed JSON is stored in "data")

  // To finish the assignment, add an error to URL and add handle the error both as
  // a second argument to then() as well as via the alternative taught in the module
});

